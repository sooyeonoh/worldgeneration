# Project 3 Prep

**For tessellating hexagons, one of the hardest parts is figuring out where to place each hexagon/how to easily place hexagons on screen in an algorithmic way.
After looking at your own implementation, consider the implementation provided near the end of the lab.
How did your implementation differ from the given one? What lessons can be learned from it?**

Answer: I didn't have enough time to think about how to place the hexagons. I learned from the lab that a useful way to organize the hexagons is to pick a starting hexagon and build other hexagons around it according to the column and row indices.

-----

**Can you think of an analogy between the process of tessellating hexagons and randomly generating a world using rooms and hallways?
What is the hexagon and what is the tesselation on the Project 3 side?**

Answer: A hexagon is a room or a hallway and a tesselation is the world created by combining the rooms and hallways.

-----
**If you were to start working on world generation, what kind of method would you think of writing first? 
Think back to the lab and the process used to eventually get to tessellating hexagons.**

Answer: I would write code to first build a room and a hallway, then figure out a way to place them, then finally build the world.

-----
**What distinguishes a hallway from a room? How are they similar?**

Answer: A hallway connects rooms together and have constant width, while rooms can have variable length and width. They are similar in that they both involve walls that enclose a floor.
