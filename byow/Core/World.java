package byow.Core;
import byow.TileEngine.TERenderer;
import byow.TileEngine.TETile;
import byow.TileEngine.Tileset;

import java.util.*;

/** The World class. Used to generate a world from which you can add rooms and hallways. */
public class World {
    private int WIDTH;
    private int HEIGHT;
    private Random RANDOM;
    private long SEED;
    private static final int MINROOMNUM = 5;
    private static final int MAXROOMNUM = 12;
    private TETile[][] world;
    private List<Room> rooms;
    private HashSet<Position> blocked;
    private Graph connections;
    private List<Hallway> hallways;

    /**
     * Constructs a random world of rooms and hallways.
     */
    public World(int width, int height, long seed) {
        WIDTH = width;
        HEIGHT = height;
        SEED = seed;
        RANDOM = new Random(seed);
        world = new TETile[width][height];
        for (int x = 0; x < world.length; x++) {
            for (int y = 0; y < world[0].length; y++) {
                world[x][y] = Tileset.NOTHING;
            }
        }
        rooms = new ArrayList<Room>();
        blocked = new HashSet<Position>();
        hallways = new ArrayList<Hallway>();
        makeRooms();
        makeHallways();
    }

    public TETile[][] getWorld() {
        return world;
    }

    /** Makes a random number of rooms. */
    public void makeRooms() {
        int numRooms = RANDOM.nextInt(MAXROOMNUM) + MINROOMNUM;
        for (int i = 0; i < numRooms; i++) {
            addRoom();
        }
        connections = new Graph(rooms);
    }

    /** Fills rooms with FLOOR tiles. */
    public void fillRooms() {
        for (Room r : rooms) {
            r.fillRoom(world);
        }
    }

    /** Fills rooms with FLOOR tiles. */
    public void fillHallways() {
        for (Hallway h : hallways) {
            h.fillHallway(world);
        }
    }

    /** Adds one room of random height and width to a random position, if there is space available. */
    public void addRoom() {
        int width = RANDOM.nextInt(Room.MAXSIDE) + Room.MINSIDE;
        int height = RANDOM.nextInt(Room.MAXSIDE) + Room.MINSIDE;
        Position pos = getRandomPosition(width, height);
        Room r = new Room(pos, width, height);
        List<Position> positions = r.getPositions();

        if (r.getTopRight().getX() > world.length || r.getBottomRight().getY() < 0
                || r.getBottomLeft().getX() < 0 || r.getTopRight().getY() > world[0].length) {
            addRoom();
            return;
        }
        for (Position p : positions) {
            if (blocked.contains(p)) {
                addRoom();
                return;
            }
        }
        for (Position p : positions) {
            world[p.getX()][p.getY()] = Tileset.WALL;
            blocked.add(p);
        }
        rooms.add(r);
    }

    /**
     * Draws the hallway onto the world.
     * DOES NOT CONNECT ROOMS TOGETHER ON THE MAP.
     * @param h The hallway
     */
    private void drawHallway(Hallway h) {
        List<Position> positionList = h.getPositions();
        for (Position p : positionList) {
            world[p.getX()][p.getY()] = Tileset.WALL;
        }
    }

    /** Makes a random number of hallways. */
    public void makeHallways() {
        for (int i = 0; i < rooms.size() - 1; i++) {
            addHallway(rooms.get(i), rooms.get(i + 1));
        }
        fillHallways();
        fillRooms();
    }

    /** Adds a hallway connecting two rooms together.
     * @param currentRoom the room from which the hallway starts.
     * @param goalRoom the room to which the hallway ends.
     * */
    public void addHallway (Room currentRoom, Room goalRoom){
        Position start = currentRoom.getRandomPosition(SEED);
        Position goal = goalRoom.getRandomPosition(SEED);

        Hallway horizontal, vertical;
        int key = RANDOM.nextInt(2);
        if (key == 0) {
            horizontal = new Hallway(start, new Position(goal.getX(), start.getY()), false);
            Position vStart = horizontal.getEnd();
            addCorner(vStart);
            vertical = new Hallway(vStart, goal, true);
        } else {
            vertical = new Hallway(start, new Position(start.getX(), goal.getY()), true);
            Position hStart = vertical.getEnd();
            addCorner(hStart);
            horizontal = new Hallway(hStart, goal, false);
        }
        drawHallway(horizontal);
        drawHallway(vertical);
        hallways.add(horizontal);
        hallways.add(vertical);
        connections.addEdge(currentRoom, goalRoom);
    }

    /** Adds a corner to a Position where hallways intersect. */
    public void addCorner (Position corner) {
        for (int x = corner.getX() - 1; x < corner.getX() + 2; x++) {
            for (int y = corner.getY() - 1; y < corner.getY() + 2; y++) {
                world[x][y] = Tileset.WALL;
            }
        }
        world[corner.getX()][corner.getY()] = Tileset.FLOOR;
    }

    /** Returns a random Position on the world. */
    public Position getRandomPosition ( int width, int height) {
        int posX = RANDOM.nextInt(world.length - width);
        int posY = RANDOM.nextInt(world[0].length - height);
        return new Position(posX, posY);
    }

    public static void main (String[]args){
        TERenderer ter = new TERenderer();
        ter.initialize(80, 30);

        World w = new World(80, 30, 1235678);
        TETile[][] randomTiles = w.getWorld();
        ter.renderFrame(randomTiles);
    }
}