package byow.Core;
import byow.TileEngine.TETile;
import byow.Core.Position;
import byow.TileEngine.Tileset;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The Room class.
 * @source Lab 12
 * */
public class Room {
    public static final int MINSIDE = 4;
    public static final int MAXSIDE = 7;

    private Position topLeft;
    private Position topRight;
    private Position bottomLeft;
    private Position bottomRight;
    private int height;
    private int width;

    public Room(Position p, int w, int h) {
        topLeft = p;
        height = h;
        width = w;
        topRight = new Position(topLeft.getX() + width, topLeft.getY());
        bottomLeft = new Position(topLeft.getX(), topLeft.getY() - height);
        bottomRight = new Position(topLeft.getX() + width, topLeft.getY() - height);
    }

    /** Returns a list of Positions of all tiles of the room. */
    public List<Position> getPositions() {
        List<Position> positions = new ArrayList<>();
        for (int x = 0; x < width + 1; x++) {
            for (int y = 0; y < height + 1; y++) {
                positions.add(new Position(topLeft.getX() + x, topLeft.getY() - y));
            }
        }
        return positions;
    }

    /** Returns a list of Positions of all FLOOR tiles of the room. */
    public List<Position> getFloorPositions() {
        List<Position> positions = new ArrayList<>();
        for (int x = 1; x < width; x++) {
            for (int y = 1; y < height; y++) {
                Position p = new Position(topLeft.getX() + x, topLeft.getY() - y);
                positions.add(p);
            }
        }
        return positions;
    }

    /** Returns a random Position inside the room. */
    public Position getRandomPosition(long seed) {
        Random r = new Random(seed + 1);
        List<Position> floor = getFloorPositions();
        return floor.get(r.nextInt(floor.size()));
    }

    /** Fills the current room with FLOOR tiles. */
    public void fillRoom(TETile[][] tiles) {
        for (Position p : getFloorPositions()) {
            tiles[p.getX()][p.getY()] = Tileset.FLOOR;
        }
    }

    public static void main(String[] args) {
        Position pos = new Position(15, 30);
        Room rm = new Room(pos, 7, 4);
        List<Position> lst = rm.getFloorPositions();
        for (Position p : lst) {
            System.out.println("X: " + p.getX() + " Y: " + p.getY());
        }
    }

    public Position getTopLeft() {
        return topLeft;
    }

    public Position getTopRight() {
        return topRight;
    }

    public Position getBottomLeft() {
        return bottomLeft;
    }

    public Position getBottomRight() {
        return bottomRight;
    }

    public int height() {
        return height;
    }

    public int width() {
        return width;
    }
}
