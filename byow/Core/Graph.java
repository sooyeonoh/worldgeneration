package byow.Core;

import java.util.*;

public class Graph {
    private HashMap<Room, List<Room>> connections;
    private HashSet<Room> connected;

    public Graph(List<Room> rooms) {
        connections = new HashMap<Room, List<Room>>();
        connected = new HashSet<Room>();
        for (Room r : rooms) {
            connections.put(r, new LinkedList<Room>());
        }
        connected = new HashSet<>();
    }

    public void addEdge(Room source, Room goal) {
        connections.get(source).add(goal);
        connections.get(goal).add(source);
        connected.add(source);
    }

    public int numConnected() {
        return connected.size();
    }

    public HashSet<Room> getConnected() {
        return connected;
    }
}
