package byow.Core;

import byow.TileEngine.TETile;
import byow.TileEngine.Tileset;

import java.util.ArrayList;
import java.util.List;;

public class Hallway {
    private int length;
    private boolean orientation;
    private boolean direction;
    private Position start;
    private Position end;
    private static final boolean VERTICAL = true;
    private static final boolean HORIZONTAL = false;
    private static final boolean DECREASING = true;
    private static final boolean INCREASING = false;

    public Hallway(Position s, Position e, boolean o) {
        start = s;
        end = e;
        if (o) {
            orientation = VERTICAL;
            length = Math.abs(end.getY() - start.getY());
            if (end.getY() - start.getY() > 0) {
                direction = INCREASING;
            } else {
                direction = DECREASING;
            }
        } else {
            orientation = HORIZONTAL;
            length = Math.abs(e.getX() - s.getX());
            if (end.getX() - start.getX() > 0) {
                direction = INCREASING;
            } else {
                direction = DECREASING;
            }
        }
    }

    /** Returns a list of Positions of all tiles of the hallway. */
    public List<Position> getPositions() {
        List<Position> positions = new ArrayList<>();
        if (orientation == HORIZONTAL) {
            for (int x = 0; x < length + 1; x++) {
                for (int y = -1; y < 2; y++) {
                    if (direction == INCREASING) {
                        positions.add(new Position(start.getX() + x, start.getY() - y));
                    } else {
                        positions.add(new Position(start.getX() - x, start.getY() - y));
                    }
                }
            }
            return positions;
        } else {
            for (int y = 0; y < length + 1; y++) {
                for (int x = -1; x < 2; x++) {
                    if (direction == INCREASING) {
                        positions.add(new Position(start.getX() - x, start.getY() + y));
                    } else {
                        positions.add(new Position(start.getX() - x, start.getY() - y));
                    }
                }
            }
            return positions;
        }
    }

    /** Returns a list of Positions of all FLOOR tiles of the hallway. */
    public List<Position> getFloorPositions() {
        List<Position> positions = new ArrayList<>();
        if (orientation == HORIZONTAL) {
            for (int x = 0; x < length; x++) {
                if (direction == INCREASING) {
                    positions.add(new Position(start.getX() + x, start.getY()));
                } else {
                    positions.add(new Position(start.getX() - x, start.getY()));
                }
            }
        } else {
            for (int y = 0; y < length; y++) {
                if (direction == INCREASING) {
                    positions.add(new Position(start.getX(), start.getY() + y));
                } else {
                    positions.add(new Position(start.getX(), start.getY() - y));
                }
            }
        }
        return positions;
    }

    /** Fills the hallway with FLOOR tiles. */
    public void fillHallway(TETile[][] tiles) {
        for (Position p : getFloorPositions()) {
            tiles[p.getX()][p.getY()] = Tileset.FLOOR;
        }
    }

    public static void main(String[] args) {
        Position end = new Position(15, 23);
        Position start = new Position(56, 12);
        Hallway h = new Hallway(start, end, true);
        List<Position> lst = h.getPositions();
        for (Position p : lst) {
            System.out.println("X: " + p.getX() + " Y: " + p.getY());
        }
    }

    public int getLength() {
        return this.length;
    }

    public boolean getOrientation() {
        return this.orientation;
    }

    public boolean getDirection() {
        return this.direction;
    }

    public Position getStart() {
        return this.start;
    }

    public Position getEnd() {
        return this.end;
    }

}
