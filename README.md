# Build Your Own World

A 2D tile-based world exploration engine with randomly generated rooms and hallways. Features include saving/loading the world, moving an avatar, and user interactivity.
